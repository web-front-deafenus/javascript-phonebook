$('document').ready(function () {
    let searchParams = new URLSearchParams(window.location.search);
    let name = '';
    if (searchParams.has('name')) {
        name = searchParams.get('name');
        let phonebook = phonebooks.find((g) => g.name === name);
        $('#number').val(phonebook.number);
        $('#name').val(phonebook.name);
        $('#email').val(phonebook.email);
        $('#address').val(phonebook.address);
    }

    $('#save').click(function () {
        if (name === '') {
            phonebooks.push({
                number: $('#number').val(),
                name: $('#name').val(),
                email: $('#email').val(),
                address: $('#address').val()
            });
        } else {
            let phonebook = phonebooks.find((g) => g.name === name);
            phonebook.number = $('#number').val();
            phonebook.name = $('#name').val();
            phonebook.email = $('#email').val();
            phonebook.address = $('#address').val();
        }
        savePhonebook(phonebooks);
        $(location).attr('href', 'index.html');
    });
});