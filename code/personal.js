$('document').ready(function () {
    var rowText;
    var content = $('#phonebook');

    for (var row of phonebooks) {
        rowText =
            `<tr>
                <td>${row.number}</td>
                <td>${row.name}</td>
                <td>${row.email}</td>
                <td>${row.address}</td>
                <td class="text-right">
                    <a class="btn btn-outline-secondary" href="phonebook.html?name=${row.name}">
                        <i class="fa fa-fw fa-edit"></i>
                    </a>
                    <button type="button" class="btn btn-outline-danger rem-row" rowid="${row.name}">
                        <i class="fa fa-fw fa-trash"></i>
                    </button>
                </td>
            </tr>`;
        content.append(rowText);
    }
    $('.rem-row').click(function () {
        let name = $(this).attr('rowid');
        savePhonebook(phonebooks.filter((g) => g.name !== name));
        location.reload();
    })
});