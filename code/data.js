function savePhonebook(phonebooks) {
    localStorage.setItem("phonebook_data", JSON.stringify(phonebooks));
}

var phonebooks = localStorage.getItem("phonebook_data");
if (localStorage.getItem("phonebook_data") == null) {
    phonebooks = [
        {
            number: 38023876656,
            name: 'Ottawa Republic',
            email: 'ottawa228@gmail.com',
            address: 'Kiev, Popular St, 20'
        },
        {
            number: 38023672656,
            name: 'Grisha Pukarin',
            email: 'griwka.pukarin@gmail.com',
            address: 'Kherson, Moldova St, 35'
        },
        {
            number: 38082874567,
            name: 'Aaron Chiba',
            email: 'chiba.admin.slit@gmail.com',
            address: 'Kiev, Moryakova St, 74'
        },
        {
            number: 38027364656,
            name: 'Fernando Manriquez',
            email: 'best.feeder@gmail.com',
            address: 'NY, 5th Ave, 20'
        },
    ];
    localStorage.setItem("phonebook_data", JSON.stringify(phonebooks));
} else {
    phonebooks = JSON.parse(phonebooks);
}